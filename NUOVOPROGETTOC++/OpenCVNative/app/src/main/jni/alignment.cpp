#include <opencv2/opencv.hpp>
#include "opencv2/xfeatures2d.hpp"
#include "opencv2/features2d.hpp"
#include <jni.h>

//wfba things
#include <iostream>
#include <cstdio>
#include <string>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/core/core.hpp"

//necessario per i messaggi di log usando c++
#include <android/log.h>
#define  LOG_TAG    "testjni"
#define  ALOG(...)  __android_log_print(ANDROID_LOG_INFO,LOG_TAG,__VA_ARGS__)
//

using namespace std;
using namespace cv;
using namespace cv::xfeatures2d;


Mat normalWFBA(vector<Mat> list_vec,bool alignActivated);
Mat improvedWFBA(vector<Mat> list_vec, bool alignActivated);
string name;
string ext;

const int MAX_FEATURES = 300;
const float GOOD_MATCH_PERCENT = 0.15f;

extern "C"

//Method responsible of the burst Registration, finding an homography. It align the images, and apply ORB and RANSAC.
JNIEXPORT void JNICALL
Java_embedded_project_com_opencvnative_Merge_alignImagesJNI(JNIEnv *env, jobject instance,
                                                         jlong addrIm1, jlong addrIm2,
                                                         jlong addrIm1Reg, jlong addrH) {

    //mesaggio di log
    //ALOG("Start of alignment method %d.", __LINE__);

    Mat& im1  = *(Mat*)addrIm1;
    Mat& im2 = *(Mat*)addrIm2;
    Mat& imResult = *(Mat*)addrIm1Reg;
    Mat& h = *(Mat*)addrH; //h non serve ritornare

    // Convert images to grayscale
    Mat im1Gray, im2Gray;
    cvtColor(im1, im1Gray, CV_BGR2GRAY);
    cvtColor(im2, im2Gray, CV_BGR2GRAY);

    // Variables to store keypoints and descriptors
    std::vector<KeyPoint> keypoints1, keypoints2;
    Mat descriptors1, descriptors2;

    // Detect ORB features and compute descriptors.
    Ptr<Feature2D> orb = ORB::create(MAX_FEATURES);
    orb->detectAndCompute(im1Gray, Mat(), keypoints1, descriptors1);
    orb->detectAndCompute(im2Gray, Mat(), keypoints2, descriptors2);

    // Match features.
    std::vector<DMatch> matches;
    Ptr<DescriptorMatcher> matcher = DescriptorMatcher::create("BruteForce-Hamming");
    matcher->match(descriptors1, descriptors2, matches, Mat());

    // Sort matches by score
    std::sort(matches.begin(), matches.end());

    // Remove not so good matches
    const int numGoodMatches = matches.size() * GOOD_MATCH_PERCENT;
    matches.erase(matches.begin() + numGoodMatches, matches.end());

    // Extract location of good matches
    std::vector<Point2f> points1, points2;

    for (size_t i = 0; i < matches.size(); i++) {
        points1.push_back(keypoints1[matches[i].queryIdx].pt);
        points2.push_back(keypoints2[matches[i].trainIdx].pt);
    }

    // Find homography with RANSAC
    h = findHomography(points2, points1, RANSAC);

    // Use homography to warp image
    warpPerspective(im2, imResult, h, im2.size());
}

extern "C"

/*Fourier Burst Accumulation:
 * -Gather images
 * - compute for each the fourier tranform
 * - Lowpass filter each one with a Gaussian filter(sigma parameter depends on imput = min(dimensione immagine lato)/ks)
 * - for each color channel, we recoumpute the accumulation avg fourier magnitude before filtering
*/
JNIEXPORT void JNICALL
Java_embedded_project_com_opencvnative_Merge_wfbaJNI(JNIEnv *env, jobject instance,
                                                           jlong addrIm1, jlong addrIm2,
                                                           jlong addrIm3, jlong addrIm4,
                                                           jlong addrIm5, jlong addrIm6,
                                                           jlong addrRes, jboolean alignOn) {

    //input acquisition
    Mat& im1 = *(Mat*)addrIm1;
    Mat& im2 = *(Mat*)addrIm2;
    Mat& im3 = *(Mat*)addrIm3;
    Mat& im4 = *(Mat*)addrIm4;
    Mat& im5 = *(Mat*)addrIm5;
    Mat& im6 = *(Mat*)addrIm6;
    //Mat& res = *(Mat*)addrRes;
    Mat& res = *(Mat*)addrRes;
    bool alignActivated = (bool) alignOn;

    //input vector
    vector<Mat> list_vec;
    list_vec.push_back(im1);
    list_vec.push_back(im2);
    list_vec.push_back(im3);
    list_vec.push_back(im4);
    list_vec.push_back(im5);
    list_vec.push_back(im6);

    res = improvedWFBA(list_vec,alignActivated);
}

extern "C"

/*Fourier Burst Accumulation:
 * -Gather images
 * - compute for each the fourier tranform
 * - Lowpass filter each one with a Gaussian filter(sigma parameter depends on imput = min(dimensione immagine lato)/ks)
 * - for each color channel, we recoumpute the accumulation avg fourier magnitude before filtering
 *
*/
JNIEXPORT void JNICALL
Java_embedded_project_com_opencvnative_Merge_wfbaFastJNI(JNIEnv *env, jobject instance,
                                                     jlong addrIm1, jlong addrIm2,
                                                     jlong addrIm3, jlong addrRes, jboolean alignOn) {

    //input acquisition
    Mat& im1 = *(Mat*)addrIm1;
    Mat& im2 = *(Mat*)addrIm2;
    Mat& im3 = *(Mat*)addrIm3;
    Mat& res = *(Mat*)addrRes;
    bool alignActivated = (bool) alignOn;

    //input vector
    vector<Mat> list_vec;
    list_vec.push_back(im1);
    list_vec.push_back(im2);
    list_vec.push_back(im3);


    res = improvedWFBA(list_vec, alignActivated);

}

//versione di wfba con dimensioni ottime per dft
Mat improvedWFBA(vector<Mat> list_vec, bool alignActivated)
{

    //dimensione ottima righe colonne per fft
    int m = getOptimalDFTSize(list_vec[0].rows);
    int n = getOptimalDFTSize(list_vec[0].cols);
    Size optimalSize(n,m);

    //parametri del wfba
    int ks = 51; //dimensione cella filtro gaussiano va fatta DISPARI
    if(min(m, n)<1000)
        ks= 21;
    const int P = 11; //parametro per elevazione a potenza


    // matrices and matrix array necessary to computation
    Mat w(optimalSize, CV_32F, Scalar(0)); //inizializzazione della matrice w, con fissate: righe, colonne, tipo float a 32 bits, inizializzata a 0.

    Mat channel_B[] = {Mat::zeros(optimalSize, CV_32F), Mat::zeros(optimalSize, CV_32F)};
    Mat channel_G[] = {Mat::zeros(optimalSize, CV_32F), Mat::zeros(optimalSize, CV_32F)};
    Mat channel_R[] = {Mat::zeros(optimalSize, CV_32F), Mat::zeros(optimalSize, CV_32F)};


    Mat wi;
    Mat wip;
    //per ogni immagine in input
    for(unsigned int i = 0; i < list_vec.size(); i++) {
        Mat wi = Mat::zeros(m, n, CV_32F);
        double sigma = min(m, n) / ks; //sigma come calcolato nel paper

        //vector<Mat [2]> vi_fft; //fft parziali diviso per canale
        Mat vi_B[] = {Mat::zeros(optimalSize, CV_32F),Mat::zeros(optimalSize, CV_32F)};
        Mat vi_G[] = {Mat::zeros(optimalSize, CV_32F),Mat::zeros(optimalSize, CV_32F)};
        Mat vi_R[] = {Mat::zeros(optimalSize, CV_32F),Mat::zeros(optimalSize, CV_32F)};


        vector<Mat> colors;//divido l'immagine nei 3 colori
        cv::split(list_vec[i], colors);

        //calcolo le 3 trasformate e le metto su vi_fft
        for (int j = 0; j < 3; j++) {
            Mat tmp,padded;
            copyMakeBorder(Mat_<float>(colors[j]),padded,0,m-colors[j].rows,0,n-colors[j].cols,BORDER_CONSTANT,Scalar::all(0));
            Mat planes[] = {padded, Mat::zeros(optimalSize, CV_32F)};
            cv::merge(planes, 2, tmp); // unisce le due matrici per fare la dft
            cv::dft(tmp, tmp); //calcolo dft e salvo su tmp


            if (j == 0) {
                cv::split(tmp, vi_B);  //salva trasformata canale B
                cv::magnitude(vi_B[0], vi_B[1], tmp); //calcolo magnitudine e salvo su tmp

            } else if (j == 1) {
                cv::split(tmp, vi_G);//salva trasformata canale G
                cv::magnitude(vi_G[0], vi_G[1], tmp); //calcolo magnitudine e salvo su tmp
            } else if (j == 2) {
                cv::split(tmp, vi_R);//salva trasformata canale R
                cv::magnitude(vi_R[0], vi_R[1], tmp); //calcolo magnitudine e salvo su tmp
            }
            GaussianBlur(tmp, tmp, Size(ks, ks), sigma);
            normalize(tmp,tmp,0,1,NORM_MINMAX);

            //al momento wi contiene la magnitudine filtrata di una immagine
            cv::addWeighted(wi, 1, tmp, 1 / 3.0, 0, wi); //accumulo media pesi

        }

        //al momento wi contiene la magnitudine (media sui canali) della i-esima immagine
        //filtro gaussiano
        //double sigma = min(list_vec[i].rows,list_vec[i].cols)/ks; //sigma come calcolato nel paper
        //double sigma = 80.0;
        //GaussianBlur( wi, wi, Size( ks, ks), sigma);
        //al momento wi contiene la magnitudine filtrata di una immagine

        cv::pow(wi, P, wip);// calcolo wi^p


        cv::multiply(wip, vi_B[0], vi_B[0]);  // vi_reale * (wi)^p
        cv::multiply(wip, vi_B[1], vi_B[1]);  // vi_immag * (wi)^p

        cv::add(channel_B[0], vi_B[0],
                channel_B[0]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti
        cv::add(channel_B[1], vi_B[1],
                channel_B[1]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti

        cv::multiply(wip, vi_G[0], vi_G[0]);  // vi_reale * (wi)^p
        cv::multiply(wip, vi_G[1], vi_G[1]);  // vi_immag * (wi)^p

        cv::add(channel_G[0], vi_G[0],
                channel_G[0]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti
        cv::add(channel_G[1], vi_G[1],
                channel_G[1]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti

        cv::multiply(wip, vi_R[0], vi_R[0]);  // wi^p * vi_reale
        cv::multiply(wip, vi_R[1], vi_R[1]);  // wi^p * vi_immag

        cv::add(channel_R[0], vi_R[0],
                channel_R[0]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti
        cv::add(channel_R[1], vi_R[1],
                channel_R[1]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti


        cv::add(w, wip, w);               //w = w + (wi)^p  accumulo i pesi globali
    }

    vector<Mat> result;//risultati parziali canale per canale

    cv::divide(channel_R[0], w, channel_R[0]); //planesUP_re = planesUP_re / w  divido componenti reali dft per i loro pesi ottenuti da accumulazione
    cv::divide(channel_R[1], w, channel_R[1]); //planesUP_imm = planesUP_imm / w  divido componenti immaginarie dft per i loro pesi ottenuti da accumulazione

    cv::divide(channel_G[0], w, channel_G[0]); //planesUP_re = planesUP_re / w  divido componenti reali dft per i loro pesi ottenuti da accumulazione
    cv::divide(channel_G[1], w, channel_G[1]); //planesUP_imm = planesUP_imm / w  divido componenti immaginarie dft per i loro pesi ottenuti da accumulazione

    cv::divide(channel_B[0], w, channel_B[0]); //planesUP_re = planesUP_re / w  divido componenti reali dft per i loro pesi ottenuti da accumulazione
    cv::divide(channel_B[1], w, channel_B[1]); //planesUP_imm = planesUP_imm / w  divido componenti immaginarie dft per i loro pesi ottenuti da accumulazione

    Mat res;
    //gestisco l'inserimentodei canali secondo l'ordine necessario. Se le immagini sono state allineate i canali sono già invertiti e quindi li inserisco come BGR anzichè RGB
    if(alignActivated){//BGR
        Mat tmpB;
        merge(channel_B,2,tmpB);//unisco re e img
        idft(tmpB,tmpB,DFT_SCALE | DFT_REAL_OUTPUT); // calcola trasformata inversa, mantiene la parte reale e scala risultato tra 0-1 per poterlo visualizzare
        result.push_back(tmpB); //salvo canale completo

        Mat tmpG;
        merge(channel_G,2,tmpG);//unisco re e img
        idft(tmpG,tmpG,DFT_SCALE | DFT_REAL_OUTPUT); // calcola trasformata inversa, mantiene la parte reale e scala risultato tra 0-1 per poterlo visualizzare
        result.push_back(tmpG); //salvo canale completo

        Mat tmpR;
        merge(channel_R,2,tmpR);//unisco re e img
        idft(tmpR,tmpR,DFT_SCALE | DFT_REAL_OUTPUT);// calcola trasformata inversa, mantiene la parte reale e scala risultato tra 0-1 per poterlo visualizzare
        result.push_back(tmpR); //salvo canale completo

        Mat out;
        cv::merge(result,out); //unisco i 3 canali risultati per avere immagine a colori
        out(Rect(0,0,list_vec[0].cols,list_vec[0].rows)).convertTo(res,CV_8UC3);

    }else{//RGB
        Mat tmpR;
        merge(channel_R,2,tmpR);//unisco re e img
        idft(tmpR,tmpR,DFT_SCALE | DFT_REAL_OUTPUT);// calcola trasformata inversa, mantiene la parte reale e scala risultato tra 0-1 per poterlo visualizzare
        result.push_back(tmpR); //salvo canale completo

        Mat tmpG;
        merge(channel_G,2,tmpG);//unisco re e img
        idft(tmpG,tmpG,DFT_SCALE | DFT_REAL_OUTPUT);// calcola trasformata inversa, mantiene la parte reale e scala risultato tra 0-1 per poterlo visualizzare
        result.push_back(tmpG); //salvo canale completo

        Mat tmpB;
        merge(channel_B,2,tmpB);//unisco re e img
        idft(tmpB,tmpB,DFT_SCALE | DFT_REAL_OUTPUT); // calcola trasformata inversa, mantiene la parte reale e scala risultato tra 0-1 per poterlo visualizzare
        result.push_back(tmpB); //salvo canale completo

        Mat out;
        cv::merge(result,out); //unisco i 3 canali risultati per avere immagine a colori
        out(Rect(0,0,list_vec[0].cols,list_vec[0].rows)).convertTo(res,CV_8UC3);
    }
    return res;
}

Mat normalWFBA(vector<Mat> list_vec, bool alignActivated)
{
    //parametri del wfba
    int ks = 51; //dimensione cella filtro gaussiano va fatta DISPARI
    if(min(list_vec[0].cols, list_vec[0].rows)<1000)
        ks= 21;
    const int P = 6; //parametro per elevazione a potenza

    // matrices and matrix array necessary to computation
    Mat w(list_vec[0].size(), CV_32F, Scalar(0)); //inizializzazione della matrice w, con fissate: righe, colonne, tipo float a 32 bits, inizializzata a 0.

    Mat channel_B[] = {Mat::zeros(list_vec[0].size(), CV_32F), Mat::zeros(list_vec[0].size(), CV_32F)};
    Mat channel_G[] = {Mat::zeros(list_vec[0].size(), CV_32F), Mat::zeros(list_vec[0].size(), CV_32F)};
    Mat channel_R[] = {Mat::zeros(list_vec[0].size(), CV_32F), Mat::zeros(list_vec[0].size(), CV_32F)};


    Mat wi;
    Mat wip;

    //per ogni immagine in input
    for(unsigned int i = 0; i < list_vec.size(); i++) {
        Mat wi = Mat::zeros(list_vec[0].rows, list_vec[0].cols, CV_32F);
        double sigma = min(list_vec[i].rows, list_vec[i].cols) / ks; //sigma come calcolato nel paper

        //vector<Mat [2]> vi_fft; //fft parziali diviso per canale
        Mat vi_B[] = {Mat::zeros(list_vec[0].size(), CV_32F),Mat::zeros(list_vec[0].size(), CV_32F)};
        Mat vi_G[] = {Mat::zeros(list_vec[0].size(), CV_32F),Mat::zeros(list_vec[0].size(), CV_32F)};
        Mat vi_R[] = {Mat::zeros(list_vec[0].size(), CV_32F),Mat::zeros(list_vec[0].size(), CV_32F)};


        vector<Mat> colors;//divido l'immagine nei 3 colori
        //Mat converted;
        //list_vec[i].convertTo(converted, CV_32FC3);
        cv::split(list_vec[i], colors);

        //calcolo le 3 trasformate e le metto su vi_fft
        for (int j = 0; j < 3; j++) {
            Mat tmp;
            Mat planes[] = {Mat_<float>(colors[j]), Mat::zeros(list_vec[i].size(), CV_32F)};
            cv::merge(planes, 2, tmp); // unisce le due matrici per fare la dft
            cv::dft(tmp, tmp); //calcolo dft e salvo su tmp


            if (j == 0) {
                cv::split(tmp, vi_B);  //salva trasformata canale B
                cv::magnitude(vi_B[0], vi_B[1], tmp); //calcolo magnitudine e salvo su tmp

            } else if (j == 1) {
                cv::split(tmp, vi_G);//salva trasformata canale G
                cv::magnitude(vi_G[0], vi_G[1], tmp); //calcolo magnitudine e salvo su tmp
            } else if (j == 2) {
                cv::split(tmp, vi_R);//salva trasformata canale R
                cv::magnitude(vi_R[0], vi_R[1], tmp); //calcolo magnitudine e salvo su tmp
            }
            GaussianBlur(tmp, tmp, Size(ks, ks), sigma);
            normalize(tmp,tmp,0,1,NORM_MINMAX);

            //al momento wi contiene la magnitudine filtrata di una immagine
            cv::addWeighted(wi, 1, tmp, 1 / 3.0, 0, wi); //accumulo media pesi

        }

        //al momento wi contiene la magnitudine (media sui canali) della i-esima immagine
        //filtro gaussiano
        //double sigma = min(list_vec[i].rows,list_vec[i].cols)/ks; //sigma come calcolato nel paper
        //double sigma = 80.0;
        //GaussianBlur( wi, wi, Size( ks, ks), sigma);
        //al momento wi contiene la magnitudine filtrata di una immagine

        cv::pow(wi, P, wip);// calcolo wi^p


        cv::multiply(wip, vi_B[0], vi_B[0]);  // vi_reale * (wi)^p
        cv::multiply(wip, vi_B[1], vi_B[1]);  // vi_immag * (wi)^p

        cv::add(channel_B[0], vi_B[0],
                channel_B[0]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti
        cv::add(channel_B[1], vi_B[1],
                channel_B[1]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti

        cv::multiply(wip, vi_G[0], vi_G[0]);  // vi_reale * (wi)^p
        cv::multiply(wip, vi_G[1], vi_G[1]);  // vi_immag * (wi)^p

        cv::add(channel_G[0], vi_G[0],
                channel_G[0]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti
        cv::add(channel_G[1], vi_G[1],
                channel_G[1]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti

        cv::multiply(wip, vi_R[0], vi_R[0]);  // wi^p * vi_reale
        cv::multiply(wip, vi_R[1], vi_R[1]);  // wi^p * vi_immag

        cv::add(channel_R[0], vi_R[0],
                channel_R[0]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti
        cv::add(channel_R[1], vi_R[1],
                channel_R[1]); //up = up + vi*(wi)^p  accumulazione pesata di questa trasformata alle precedenti


        cv::add(w, wip, w);               //w = w + (wi)^p  accumulo i pesi globali
    }

    vector<Mat> result;//risultati parziali canale per canale

    cv::divide(channel_R[0], w, channel_R[0]); //planesUP_re = planesUP_re / w  divido componenti reali dft per i loro pesi ottenuti da accumulazione
    cv::divide(channel_R[1], w, channel_R[1]); //planesUP_imm = planesUP_imm / w  divido componenti immaginarie dft per i loro pesi ottenuti da accumulazione

    cv::divide(channel_G[0], w, channel_G[0]); //planesUP_re = planesUP_re / w  divido componenti reali dft per i loro pesi ottenuti da accumulazione
    cv::divide(channel_G[1], w, channel_G[1]); //planesUP_imm = planesUP_imm / w  divido componenti immaginarie dft per i loro pesi ottenuti da accumulazione

    cv::divide(channel_B[0], w, channel_B[0]); //planesUP_re = planesUP_re / w  divido componenti reali dft per i loro pesi ottenuti da accumulazione
    cv::divide(channel_B[1], w, channel_B[1]); //planesUP_imm = planesUP_imm / w  divido componenti immaginarie dft per i loro pesi ottenuti da accumulazione

    Mat res;
    //gestisco l'inserimentodei canali secondo l'ordine necessario. Se le immagini sono state allineate i canali sono già invertiti e quindi li inserisco come BGR anzichè RGB
    if(alignActivated){//BGR
        Mat tmpB;
        merge(channel_B,2,tmpB);//unisco re e img
        idft(tmpB,tmpB,DFT_SCALE | DFT_REAL_OUTPUT); // calculate the inverse Discrete Fourier Transform of up and put it in res con scala reale
        result.push_back(tmpB); //salvo canale completo

        Mat tmpG;
        merge(channel_G,2,tmpG);//unisco re e img
        idft(tmpG,tmpG,DFT_SCALE | DFT_REAL_OUTPUT); // calculate the inverse Discrete Fourier Transform of up and put it in res con scala reale
        result.push_back(tmpG); //salvo canale completo

        Mat tmpR;
        merge(channel_R,2,tmpR);//unisco re e img
        idft(tmpR,tmpR,DFT_SCALE | DFT_REAL_OUTPUT); // calculate the inverse Discrete Fourier Transform of up and put it in res con scala reale
        result.push_back(tmpR); //salvo canale completo

        Mat out;
        cv::merge(result,out); //unisco i 3 canali risultati per avere immagine a colori
        out.convertTo(res,CV_8UC3);

    }else{//RGB
        Mat tmpR;
        merge(channel_R,2,tmpR);//unisco re e img
        idft(tmpR,tmpR,DFT_SCALE | DFT_REAL_OUTPUT); // calculate the inverse Discrete Fourier Transform of up and put it in res con scala reale
        result.push_back(tmpR); //salvo canale completo

        Mat tmpG;
        merge(channel_G,2,tmpG);//unisco re e img
        idft(tmpG,tmpG,DFT_SCALE | DFT_REAL_OUTPUT); // calculate the inverse Discrete Fourier Transform of up and put it in res con scala reale
        result.push_back(tmpG); //salvo canale completo

        Mat tmpB;
        merge(channel_B,2,tmpB);//unisco re e img
        idft(tmpB,tmpB,DFT_SCALE | DFT_REAL_OUTPUT); // calculate the inverse Discrete Fourier Transform of up and put it in res con scala reale
        result.push_back(tmpB); //salvo canale completo

        Mat out;
        cv::merge(result,out); //unisco i 3 canali risultati per avere immagine a colori
        out.convertTo(res,CV_8UC3);
    }
    return res;
}

