package embedded.project.com.opencvnative;

import android.Manifest;
import android.content.ClipData;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    // Used when request read external storage permission.
    private final static int REQUEST_PERMISSION_READ_EXTERNAL = 2;

    //Total number of pictures to be selected
    private static int TOTAL_PICS;

    //Uri List
    private ArrayList<Uri> uriList = new ArrayList<>();

    //Initialize a Toast to null
    private Toast mToast = null;

    //used when request to pick multiple images
    int PICK_IMAGE_MULTIPLE = 1;

    //total selected pictures
    int selectedPics = 0;

    //Alignment condition
    private boolean alignmentActivated;

    //Speed condition
    private boolean fast;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("DeBlur");

        //initialize condition variables
        fast = false;
        alignmentActivated = false;
        TOTAL_PICS = 6;

        //Method used in checkbox validation
        final Switch alignmentSwitch = (Switch)findViewById(R.id.switch_alignment);

        alignmentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //If the switch is on, "isChecked"=true. False if the switch is Off. So i set the control variable "alignmentActivated".
                alignmentActivated= isChecked;
                if(alignmentActivated) {
                    alignmentSwitch.setText("ALIGN ON");
                }else{
                    alignmentSwitch.setText("ALIGN OFF");
                }
            }
        });

        //Method to choose the speed. Fast = 3 pics, Slow = 6 pics
        final Switch speedSwitch = (Switch)findViewById(R.id.switch_speed);

        speedSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //If the switch is on, "isChecked"=true. False if the switch is Off. So i set the control variable "alignmentActivated".
                fast = isChecked;
                if(fast) {
                    speedSwitch.setText("FAST");
                    TOTAL_PICS = 3;
                }else{
                    speedSwitch.setText("SLOW");
                    TOTAL_PICS = 6;
                }
            }
        });


        // Get browse image button.
        Button choosePictureButton = (Button)findViewById(R.id.galleryButton);
        choosePictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uriList.size() >= TOTAL_PICS){

                    //avoid toast accumulation
                    if (mToast != null) mToast.cancel();
                    mToast = Toast.makeText(getApplicationContext(), "You already selected " + TOTAL_PICS + " pictures", Toast.LENGTH_LONG);
                    mToast.show();
                }
                else {

                    // Because camera app returned uri value is something like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
                    // So if show the camera image in image view, this app require below permission.
                    int readExternalStoragePermission = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE);
                    if (readExternalStoragePermission != PackageManager.PERMISSION_GRANTED) {
                        String requirePermission[] = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        ActivityCompat.requestPermissions(MainActivity.this, requirePermission, REQUEST_PERMISSION_READ_EXTERNAL);
                    } else {
                        openPictureGallery();
                    }
                }
            }
        });

        // Get show user selected images button.
        Button showSelectedPictureButton = (Button)findViewById(R.id.showButton);

        // When click this button. It will go to Merge activity.
        showSelectedPictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                //select 3 or 6 pictures
                if (uriList.size()< TOTAL_PICS){

                    //remaining pics
                    int remainingPicsNum = TOTAL_PICS - uriList.size();

                    //avoid toast accumulation
                    if (mToast != null) mToast.cancel();
                    mToast = Toast.makeText(getApplicationContext(), "Select " + remainingPicsNum + " more pictures", Toast.LENGTH_LONG);
                    mToast.show();
                }

                //go to Merge activity
                else {
                    final Intent myIntent = new Intent(view.getContext(), Merge.class);
                    myIntent.putParcelableArrayListExtra("uriList", uriList);
                    myIntent.putExtra("condition", alignmentActivated);
                    myIntent.putExtra("speed", fast);
                    startActivity(myIntent);
                }
            }
        });
    }

    //Opens the gallery
    private void openPictureGallery()
    {
        // Create an intent.
        Intent openAlbumIntent = new Intent();

        // Only show images in the content chooser.
        // If you want to select all type data then openAlbumIntent.setType("*/*");
        // Must set type for the intent, otherwise there will throw android.content.ActivityNotFoundException: No Activity found to handle Intent { act=android.intent.action.GET_CONTENT }
        openAlbumIntent.setType("image/*");

        //allow multiple selection
        openAlbumIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);

        // Set action, this action will invoke android os browse content app.
        openAlbumIntent.setAction(Intent.ACTION_GET_CONTENT);

        // Start the activity.
        startActivityForResult(Intent.createChooser(openAlbumIntent,"Select Picture"), 1);
    }

    //Calls the camera
    public void cameraCall(View view) {

        //if 6 pictures are already selected, you get a toast message
        if (uriList.size() >= TOTAL_PICS){
            //avoid toast accumulation
            if (mToast != null) mToast.cancel();
            mToast = Toast.makeText(getApplicationContext(), "You already selected " + TOTAL_PICS + " pictures", Toast.LENGTH_LONG);
            mToast.show();
        }

        //opens camera
        else {
            Intent intent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
            startActivityForResult(intent, 0);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            // When an Image is picked
            if (requestCode == PICK_IMAGE_MULTIPLE && resultCode == RESULT_OK
                    && null != data) {

                // Get the Image from data
                if(data.getData()!=null){

                    Uri mImageUri=data.getData();
                    uriList.add(mImageUri);

                    //avoid toast accumulation
                    if (mToast != null) mToast.cancel();
                    mToast = Toast.makeText(getApplicationContext(), "Selected Images: " + uriList.size(), Toast.LENGTH_LONG);
                    mToast.show();

                    selectedPics++;

                } else {
                    if (data.getClipData() != null) {
                        ClipData mClipData = data.getClipData();

                        int alreadyStored = uriList.size();

                        for(int i=0; i < mClipData.getItemCount(); i++) {

                            if (i<TOTAL_PICS-alreadyStored) {

                                ClipData.Item item = mClipData.getItemAt(i);
                                Uri uri = item.getUri();

                                //notice that the pick order is not guaranteed
                                uriList.add(uri);

                                //avoid toast accumulation
                                if (mToast != null) mToast.cancel();
                                mToast = Toast.makeText(getApplicationContext(), "Selected Images: " + uriList.size(), Toast.LENGTH_LONG);
                                mToast.show();
                            }
                            selectedPics++;
                        }

                        Log.v("LOG_TAG", "Selected Images" + uriList.size());
                    }
                }
            }


            if (selectedPics > TOTAL_PICS) {
                //avoid toast accumulation
                int ignoredPics = selectedPics - TOTAL_PICS;
                if (mToast != null) mToast.cancel();
                mToast = Toast.makeText(getApplicationContext(), "You selected too many pictures. \n " + ignoredPics + " of them will be ignored", Toast.LENGTH_LONG);
                mToast.show();

                //avoid toast when press back
                selectedPics = TOTAL_PICS;
            }

        } catch (Exception e) {

            //avoid toast accumulation
            if (mToast != null) mToast.cancel();
            mToast = Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG);
            mToast.show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /* After user choose grant read external storage permission or not. */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode==REQUEST_PERMISSION_READ_EXTERNAL)
        {
            if(grantResults.length > 0)
            {
                int grantResult = grantResults[0];
                if(grantResult == PackageManager.PERMISSION_GRANTED)
                {
                    // If user grant the permission then open choose image popup dialog.
                    openPictureGallery();
                }else
                {
                    //avoid toast accumulation
                    if (mToast != null) mToast.cancel();
                    mToast = Toast.makeText(getApplicationContext(), "You denied read external storage permission.", Toast.LENGTH_LONG);
                    mToast.show();
                }
            }
        }
    }
}
