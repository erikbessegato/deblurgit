package embedded.project.com.opencvnative;

import android.content.Intent;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Test extends AppCompatActivity {

    //Total number of pictures to be selected
    private static int TOTAL_PICS;

    //List of selected pictures URIs
    private ArrayList<Uri> uriList = new ArrayList<>();

    //List of ImageViews
    private ArrayList<ImageView> iwList = new ArrayList<>();

    //List of ImageViews names
    private ArrayList<String> iwNameList = new ArrayList<>();

    //List of ImageViews IDs
    private ArrayList<Integer> iwIdList = new ArrayList<>();

    //Final picture URI
    private Uri finalPictureUri;

    //Speed condition
    private boolean fast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        setTitle("Aligned Pictures");

        //initialize speed condition
        fast = false;

        //initialize total pictures number
        TOTAL_PICS = 6;

        //get uri from main activity
        uriList = getIntent().getParcelableArrayListExtra("uriTestList");

        // Get the Uri of the final picture from the Merge Activity
        finalPictureUri = getIntent().getParcelableExtra("finalPictureUri");

        //get the speed
        fast = getIntent().getExtras().getBoolean("speed");

        //set the total pics according to the selected speed
        if(fast)
            TOTAL_PICS = 3;

        //fill all the lists
        for (int i = 0; i < TOTAL_PICS; i++) {

            //set imgView names
            iwNameList.add("timgView" + i);

            //get imgView IDs
            iwIdList.add(getResources().getIdentifier(iwNameList.get(i), "id", getPackageName()));

            //add imgViews to a list
            iwList.add((ImageView) findViewById(iwIdList.get(i)));

            //create thumbnails
            Picasso.get().load(uriList.get(i)).resize(500,500).centerCrop().into(iwList.get(i));

            //index to make onClick method work
            final int j=i;

            //open gallery when touch a thumbnail
            iwList.get(j).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent tIntent = new Intent();
                    tIntent.setAction(android.content.Intent.ACTION_VIEW);
                    tIntent.setDataAndType(uriList.get(j), "image/*");
                    startActivity(tIntent);
                }
            });
        }

        //button to see to the final picture
        Button finalPicButton = (Button)findViewById(R.id.finalPicButton);
        finalPicButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), Result.class);
                myIntent.putExtra("finalPictureUri", finalPictureUri);
                startActivityForResult(myIntent, 0);
            }

        });
    }
}
